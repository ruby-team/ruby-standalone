-include config.mk
ifeq ($(VENDORINSTALL),)
	PREFIX = /usr/local
else
	PREFIX = /usr
endif
RUBY ?= $(shell ruby -e 'puts RbConfig::CONFIG["ruby_install_name"]')
RUBY_VERSION := $(patsubst ruby%,%,$(RUBY))
BINSTUBS := $(shell dpkg -L $(RUBY) | grep /usr/bin/ | grep -v /usr/bin/ruby | xargs -n 1 basename --suffix=$(RUBY_VERSION))
BINARIES = ruby $(BINSTUBS)
MANPAGES = ruby-standalone.1.gz
GENERATED = ruby-standalone
ifeq ($(SOURCE_DATE_EPOCH),)
	SOURCE_DATE_EPOCH = $(shell date +%s)
endif
BUILDDATE = $(shell LANG=C LC_ALL=C date -d @$(SOURCE_DATE_EPOCH) +'%Y-%m-%d')

include buildflags.mk

LIBDIR = $(PREFIX)/lib/ruby-standalone/lib
CFLAGS += $(shell pkg-config --cflags ruby-$(RUBY_VERSION)) -DRUBY_STANDALONE_LIB='"$(LIBDIR)"'
LDFLAGS += $(shell pkg-config --libs ruby-$(RUBY_VERSION))

all: $(BINARIES) $(GENERATED) $(MANPAGES)
	@echo "Ruby package: " $(RUBY)
	@echo "Ruby version: " $(RUBY_VERSION)
	@echo "    Programs: " $(BINSTUBS)

ruby: ruby.o
	$(CC) -o $@ $< $(LDFLAGS)

$(MANPAGES): %.1.gz: %.1
	$(RM) $@
	(cat $< | gzip - > $@) || ($(RM) $@; false)

buildflags.mk:
	dpkg-buildflags --export=make > $@

$(BINSTUBS):
	@printf "#!/usr/bin/env ruby\nload '/usr/bin/$@$(RUBY_VERSION)'\n" > $@ || ($(RM) $@; false)
	chmod +x $@

$(GENERATED): % : %.in
	sed -e 's#@@prefix@@#$(PREFIX)#g' $< > $@ || ($(RM) $@; false)
	if [ "$$(basename $@ .rb)" = "$@" ]; then chmod +x $@; fi

ruby-standalone.1: ruby-standalone.md
	ronn --roff \
		--section=1 \
		--date:"$(BUILDDATE)" \
		$<
	sed -i -e 's/\.SH\s*ruby.-standalone/.SH NAME\nruby\\-standalone/' $@

ruby-standalone.md: README.md
	cp $< $@

which-ruby:
	@echo $(RUBY)

install: all
	install -d $(DESTDIR)$(PREFIX)/bin
	install -d $(DESTDIR)$(PREFIX)/lib/ruby-standalone/bin
	install -d $(DESTDIR)$(PREFIX)/share/man/man1
	install -m 0755 $(BINARIES) $(DESTDIR)$(PREFIX)/lib/ruby-standalone/bin
	install -m 0755 ruby-standalone $(DESTDIR)/$(PREFIX)/bin
	install -m 0644 $(MANPAGES) $(DESTDIR)$(PREFIX)/share/man/man1
	install -d $(DESTDIR)/$(LIBDIR)/rubygems/defaults
	install -m 0644 lib/debian_ruby_standalone.rb $(DESTDIR)/$(LIBDIR)
	install -m 0644 lib/rubygems/defaults/operating_system.rb \
		$(DESTDIR)/$(LIBDIR)/rubygems/defaults/

distclean clean:
	$(RM) $(BINARIES) $(MANPAGES) ruby-standalone ruby-standalone.md buildflags.mk *.1 *.o

test: all
	if [ -x /usr/bin/ruby -a -x /usr/bin/shunit2 ]; then ./run-tests; fi
