ruby-standalone(1) - use (only) the Ruby interpreter from Debian
================================================================

## SYNOPSIS

**ruby-standalone**  
**ruby-standalone** [_PROGRAM_] [_ARGS_]

## DESCRIPTION

ruby-standlone allows one to use the Ruby interpreter provided by Debian (and
thus get security support for stable releases), without having to also use Ruby
libraries and applications from Debian.

When called with no arguments, `ruby-standalone` will spawn a new shell, where
all of the standard Ruby programs (ruby, erb, gem, irb, rdoc, ri, testrb) in
`$PATH` will never use code from Debian-provided packages.

Otherwise, _PROGRAM_ will be called with _ARGS_ as arguments, in that same
context.

**Caveat:** if _PROGRAM_ is not installed as a Rubygem and is installed by a
Debian package, the Debian package version _will_ be used.

## USE CASES

ruby-standalone makes it possible to:

- install `vagrant` from the Debian repository and at the sime time develop a
  Ruby web application that needs gem versions different than the ones that
  were pulled in from the Debian repository when `vagrant` was installed.

- install `chef` from the Debian repository on a node that will host an
  application that needs different gem versions than the ones `chef` needs.

- on the same server, host `redmine` installed from the Debian repository and
  in-house Rails application that needs a different version of Rails than the
  one Debian provides.

## LICENSE

Copyright © 2014-2020 Antonio Terceiro <terceiro@debian.org>

ruby-standalone is licensed under the same terms as Ruby itself. See the file
COPYING for details.
