$LOAD_PATH.reject! do |entry|
  entry =~ /vendor_ruby/
end

ENV["DEBIAN_RUBY_STANDALONE"] = "1"

require 'rubygems'
