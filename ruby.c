#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <ruby.h>

int
main(int argc, char **argv)
{
  char **newargv;
  int i;
  newargv = malloc(sizeof(char *) * (argc + 3));
  newargv[0] = argv[0];
  newargv[1] = "-I" RUBY_STANDALONE_LIB;
  newargv[2] = "--disable=gem";
  newargv[3] = "-rdebian_ruby_standalone";
  for (i = 1; i < argc; i++) {
    newargv[i+3] = argv[i];
  }
  argc += 3;

  ruby_init();
  ruby_sysinit(&argc, &newargv);
  {
    RUBY_INIT_STACK;
    ruby_init();
    return ruby_run_node(ruby_options(argc, newargv));
  }
}
